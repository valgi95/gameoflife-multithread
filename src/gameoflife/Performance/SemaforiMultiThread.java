package gameoflife.Performance;

import gameoflife.controller.Controller;
import gameoflife.controller.ControllerImpl;

public class SemaforiMultiThread {
    public static void main(String s[]){
        int cores=Runtime.getRuntime().availableProcessors()+1;


        Controller c1=new ControllerImpl(false,true,false,cores);
        c1.initialize(5000,5000,null);
        System.out.println("Sincronizzazione con semafori, "+cores+" thread:");
        c1.start(10);

    }
}
