package gameoflife.viewextremecanvas;

import gameoflife.controller.Controller;
import gameoflife.controller.ControllerImpl;
import gameoflife.model.MatrixImpl;
import gameoflife.viewcanvas.FxControllerCanvas;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class View extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(this.getClass().getResource("fxCanvasExtreme.fxml"));
        Scene sc = new Scene(root);
        primaryStage.setScene(sc);
        primaryStage.show();
    }

    public static void main(String[] s){
        System.out.println("Lancio la view");
        launch(s);
        System.out.println("Lanciata la view");
    }
}
