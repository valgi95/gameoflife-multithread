package gameoflife.model;

import gameoflife.controller.MatrixHandler;

import java.util.concurrent.Semaphore;

public class ThreadImpl extends java.lang.Thread {

    private int id;
    private MatrixHandler mh;
    private Integer line;
    private boolean isSyncronize,isSemaphore,isWait;
    private volatile boolean running = true;//volatile 
        public ThreadImpl (int id,boolean isSyncronize,boolean isSemaphore,boolean isWait){
            this.id=id;
            this.mh=MatrixHandler.getIstance();
            this.isSemaphore=isSemaphore;
            this.isSyncronize=isSyncronize;
            this.isWait=isWait;
        }
        
        public void stopThread() {
        	this.running=false;
        }
        
        @Override
        public void  run(){
            if (isWait) {
                try {
                    this.monitorMethod();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }else if (isSyncronize) this.syncronizeMethod();
            else if(isSemaphore) {
                try {
                    this.semaforoMethod();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        private void syncronizeMethod(){
            line=mh.getNextCellSyncronize();
            int n=MatrixHandler.getIstance().getN();
            while (line!=null){
                for (int i=0;i<n;i++) {
                    if (this.running) {
                        //setTrue(cell.getFirst(),cell.getSecond());
                        calculateCell(line, i);

                    } else {
                        // System.out.println("tread terminato");
                        return;
                    }
                }
                line = mh.getNextCellSyncronize();
            }
        }
    private void semaforoMethod() throws InterruptedException {
            mh.getSemaphore().acquire();
        line=mh.getNextCellSemaforo();
        mh.getSemaphore().release();
        int n=MatrixHandler.getIstance().getN();
        while (line!=null){
            for (int i=0;i<n;i++) {
                if (this.running) {
                    //setTrue(cell.getFirst(),cell.getSecond());
                    calculateCell(line, i);

                } else {
                    // System.out.println("tread terminato");
                    return;
                }
            }
                mh.getSemaphore().acquire();
                line=mh.getNextCellSemaforo();
                mh.getSemaphore().release();

        }
    }
        private void stampaInfo(String s){
             //System.out.println("Thread"+this.id+":"+s);
        }
    private void monitorMethod() throws InterruptedException {
            int n=MatrixHandler.getIstance().getN();
            Boolean lock=mh.getMonitor();
            //stampaInfo("attendo di prendere la lock");
            synchronized (lock) {
              //  stampaInfo("ho preso la lock");

            while (!mh.getRisorsaLibera()){
                //stampaInfo("vado in wait");
                lock.wait();
            }
            //stampaInfo("esco dalla wait");
            mh.setRisorsaLibera(false);
        }
        line = mh.getNextCellMonitor();
            while (line != null) {
                for (int i=0;i<n;i++) {
                    if (this.running) {
                        //setTrue(cell.getFirst(),cell.getSecond());
                        calculateCell(line, i);

                    } else {
                        // System.out.println("tread terminato");
                        return;
                    }
                }
                    synchronized (lock) {
                        while (!mh.getRisorsaLibera()){
                //            stampaInfo("vado in wait");
                            lock.wait();
                        }
                        mh.setRisorsaLibera(false);
                    }
                    line = mh.getNextCellMonitor();

            }

    }

        
        private void calculateCell(int m,int n){
            int CountLiveCell=this.calculateLiveNearCell(m,n);
            if (mh.getCell(m,n)){//è viva!!

                if (CountLiveCell<2) this.mh.setCell(m,n,false);                    //prima condizione (ISOLAMENTO)
                if (CountLiveCell==2 || CountLiveCell==3) this.mh.setCell(m,n,true);//seconda condizione (SOPRAVVIVE)
                if (CountLiveCell>3) this.mh.setCell(m,n,false);                    //terza condizione (SOVRAPPOPOLAMENTO)

            }else{//è morta
                if (CountLiveCell==3) this.mh.setCell(m,n,true);                    //quarta condizione (RIPRODUZIONE)
                else this.mh.setCell(m,n,false);                                    //quinta condizione (RESTA MORTA)
            }
        }
        private int calculateLiveNearCell(int m,int n){
            int count=0;
            if ((m+1)<this.mh.getM()) if(mh.getCell(m+1,n)) count++;                         //+1,0
            if ((n+1)<this.mh.getN()) if (mh.getCell(m,n+1)) count++;                        //0,+1
            if ((m+1)<this.mh.getM() && n+1<this.mh.getN() && mh.getCell(m+1,n+1)) count++;  //+1,+1
            if ((m+1)<this.mh.getM() && (n)>0 && mh.getCell(m+1,n-1)) count++;               //+1,-1
            if ((m)>0 && n+1<this.mh.getN() && mh.getCell(m-1,n+1)) count++;                 //-1,+1
            if ((m)>0 && mh.getCell(m-1,n)) count++;                                         //-1,0
            if ((n)>0 && mh.getCell(m,n-1)) count++;                                         //0,-1
            if ((n)>0 && (m)>0 && mh.getCell(m-1,n-1)) count++;                              //-1,-1
            return count;
        }
    }
