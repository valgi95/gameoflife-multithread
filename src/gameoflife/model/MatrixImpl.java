package gameoflife.model;

final public class MatrixImpl implements Matrix{
    private final int M;             // number of rows
    private final int N;             // number of columns
    private final boolean[][] matrix;   // M-by-N array



    // create M-by-N matrix of 0's
    public MatrixImpl(int M, int N) {
        this.M = M;
        this.N = N;
        matrix = new boolean[M][N];
    }
    public boolean isAllFalse (){
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++)
                if (matrix[i][j])return false;
        }
        return true;
    }
    public void show() {
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++)
                if (matrix[i][j])System.out.print("X ");
                else System.out.print("O ");
            System.out.println();
        }
    }
    public static Matrix randomMatrix(int M, int N) {
        MatrixImpl A = new MatrixImpl(M, N);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                A.matrix[i][j] = ((int)(Math.random()*10)%2 == 1);
        return A;
    }

    public static Matrix falseMatrix (int M, int N){
        MatrixImpl A = new MatrixImpl(M, N);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                A.matrix[i][j] = false;
        return A;
    }
    public boolean getCell (int M, int N){
        return matrix[M][N];
    }
    public void setCell (int M, int N,boolean value){
        matrix[M][N]= value;
    }

}