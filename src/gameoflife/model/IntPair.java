package gameoflife.model;
/**
 * 
 * @author ricardo soro
 * Questa classe gestisce una coppia di interi
 *
 */
public interface IntPair {
	
	/**
	 * 
	 * @return il primo elemento della coppia
	 */
    public int getFirst();
    
    /**
     * 
     * @return il secondo elemento della coppia
     */
    public int getSecond();

}
