package gameoflife.model;

public class IntPairImpl implements IntPair{
        private int first;
        private int second;

        public IntPairImpl (int first, int second) {
            this.first    = first;
            this.second  = second;
        }
        public int getFirst(){
            return this.first;
        }
        public int getSecond(){
            return this.second;
        }
}
