package gameoflife.controller;

import gameoflife.viewextremecanvas.FxControllerCanvasExtreme;

/**
 * 
 * @author thesh
 *
 */
public interface Controller {

	/**
	 * 
	 * @param m
	 * @param n
	 */
	public void initialize(int m, int n, FxControllerCanvasExtreme viewHandler);
	
	/**
	 * 
	 * @param generazoni
	 */
	public void start(int generazoni);
	
	/**
	 * 
	 */
	public void reset ();
	
	/**
	 * 
	 */
	public void pause ();

	/**
	 * 
	 */
	public void resume ();
	
	/**
	 * 
	 * @return
	 */
	public int getRoud();

	/**
	 *
	 */
	
	
}
