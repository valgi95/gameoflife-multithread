package gameoflife.view;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;


public class view extends Application {



    // resources


    @Override
    public void start(Stage primaryStage) throws Exception {
       Parent root = FXMLLoader.load(getClass().getResource("Griglia.fxml"));
       primaryStage.setScene(new Scene(root));
       primaryStage.show();


    }

    public static void main(String[] args){ launch(args);}
}
