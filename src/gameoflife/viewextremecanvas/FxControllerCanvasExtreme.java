package gameoflife.viewextremecanvas;

import gameoflife.controller.Controller;
import gameoflife.controller.ControllerImpl;
import gameoflife.viewcanvas.FxControllerCanvas;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

import java.util.*;

public class FxControllerCanvasExtreme {

    private final static Integer NUMBOFCANVASPERLINE = 4;
    public static final Integer NUMBOFCELL = 200;
    private static final Double SIZEOFCELL = 8.0;
    private Controller controller = new ControllerImpl(true,false,false,Runtime.getRuntime().availableProcessors());

    private static final String START = "Start";
    private static final String STOP = "Stop";
    private static final String CONTINUE = "Continue";

    private boolean isFirstTime = true;

    private List<myPair<myPair<Integer, Integer>,myPair<Integer, Integer>>> canvasCoordination = new ArrayList<>();
    private List<Canvas> canvas = new ArrayList<>();
    @FXML
    private GridPane gridpane;
    @FXML
    private Button btn_start;
    @FXML
    private TextArea generazioni;
    @FXML
    private Label lbl_generazioni;
    @FXML
    private Label lbl_cellevive;

    private boolean debug = false;


    @FXML
    private void initialize(){
        this.computeCanvasCoordination();
        this.assignActions();
        for(int i = 0; i < NUMBOFCANVASPERLINE; i++){
            for(int r = 0; r < NUMBOFCANVASPERLINE; r++){
                Canvas cv = new Canvas();
                cv.setHeight(computeHeightOfCanvas(r, i));
                cv.setWidth(computeWidthOfCanvas(r, i));
                canvas.add((r+(i*NUMBOFCANVASPERLINE)),cv);

                if(debug) {
                    cv.getGraphicsContext2D().fillRect(5.0, 5.0, computeWidthOfCanvas(r, i)- 5.0, computeHeightOfCanvas(r, i) - 5.0);
                    System.out.println(canvasCoordination.get(r + i * NUMBOFCANVASPERLINE));
                    System.out.println("Altezza:" + computeHeightOfCanvas(r, i));
                    System.out.println("Largezza:" + computeWidthOfCanvas(r, i));
                }
                gridpane.add(cv, r, i);
            }
        }
        System.out.println("Inizializzo controller");
        controller.initialize(NUMBOFCELL, NUMBOFCELL, this);
        System.out.println("Inizializzato controller");
    }


    private void assignActions(){
        this.btn_start.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(btn_start.getText().equals(START)) {
                    int g = Integer.parseInt(generazioni.getText());
                    System.out.println("Lancio start prima volta");
                    controller.start(g);
                    isFirstTime = false;
                    btn_start.setText(STOP);
                }else if (btn_start.getText().equals(STOP)){
                    System.out.println("Lancio pause");
                    controller.pause();
                    btn_start.setText(CONTINUE);
                    lbl_generazioni.setText("Generazione: "+ controller.getRoud());
                }else{
                    controller.resume();
                    btn_start.getText().equals(START);
                }

            }
        });
        if(debug)
            this.gridpane.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    System.out.println("Mouse click: X: "+ event.getX() + " Y: "+ event.getY());
                }
            });
    }


    private void computeCanvasCoordination(){
        if(NUMBOFCELL % NUMBOFCANVASPERLINE == 0){
            int step = NUMBOFCELL / NUMBOFCANVASPERLINE;
            for(int i = 0; i < NUMBOFCANVASPERLINE ; i++){
                for(int r = 0; r < NUMBOFCANVASPERLINE ; r++) {
                    this.canvasCoordination.add(new myPair<>(new myPair<>(r * step, i * step),
                            new myPair<>((r+1) * step, (i+1) * step)));
                }
            }
        }else{
            System.out.println(""+ (9/4));
            int step = NUMBOFCELL / NUMBOFCANVASPERLINE;
            for(int i = 0; i < NUMBOFCANVASPERLINE ; i++){
                for(int r = 0; r < NUMBOFCANVASPERLINE ; r++) {
                    int finalstepX = (r+1)*step ;
                    int finalstepY = (i+1)*step;

                    if(i == (NUMBOFCANVASPERLINE) - 1){
                        finalstepY += NUMBOFCELL%NUMBOFCANVASPERLINE;
                    }
                    if(r == (NUMBOFCANVASPERLINE) - 1){
                        finalstepX += NUMBOFCELL%NUMBOFCANVASPERLINE;
                    }
                    this.canvasCoordination.add(new myPair<>(new myPair<>(r * step, i * step),
                            new myPair<>(finalstepX, finalstepY)));
                }
            }
        }
    }


    private Double computeHeightOfCanvas(int x, int y){
        Double height = ( canvasCoordination.get(x+y*NUMBOFCANVASPERLINE).getY().getY()
                -canvasCoordination.get(x+y*NUMBOFCANVASPERLINE).getX().getY())* SIZEOFCELL;
        return height;
    }

    private Double computeWidthOfCanvas(int x, int y){
        Double width = ( canvasCoordination.get(x+y*NUMBOFCANVASPERLINE).getY().getX()
                -canvasCoordination.get(x+y*NUMBOFCANVASPERLINE).getX().getX())* SIZEOFCELL;
        return width;
    }

    public void paint(final int x,final int y){

        int index = getIndexOfCanvas(x, y);

        Canvas cv = canvas.get(index);
        myPair<myPair<Integer, Integer>, myPair<Integer, Integer>> coordinates = canvasCoordination.get(index);
        int offsetX = x - coordinates.getX().getX();
        int offsetY = y - coordinates.getX().getY();
        cv.getGraphicsContext2D().setFill(Color.BLUE);
        cv.getGraphicsContext2D().fillRect(offsetX*SIZEOFCELL, offsetY*SIZEOFCELL, SIZEOFCELL, SIZEOFCELL);
        if(debug) {
            System.out.println("Cella " + x + ", " + y + " : indice " + index);
            System.out.println("Disegnato rettangolo cella: " + x + ", " + y);
            System.out.println("Offset: " + offsetX + ", " + offsetY);
        }
    }

    public void cleanCell(final int x, final int y){
        int index = getIndexOfCanvas(x, y);

        Canvas cv = canvas.get(index);
        myPair<myPair<Integer, Integer>, myPair<Integer, Integer>> coordinates = canvasCoordination.get(index);
        int offsetX = x - coordinates.getX().getX();
        int offsetY = y - coordinates.getX().getY();
        cv.getGraphicsContext2D().setFill(Color.WHITE);
        cv.getGraphicsContext2D().fillRect(offsetX*SIZEOFCELL, offsetY*SIZEOFCELL, SIZEOFCELL, SIZEOFCELL);
        if(debug) {
            System.out.println("Cella " + x + ", " + y + " : indice " + index);
            System.out.println("Disegnato rettangolo cella: " + x + ", " + y);
            System.out.println("Offset: " + offsetX + ", " + offsetY);
        }
    }

    public void setGenerazioni(int generazione){
        lbl_generazioni.setText("Generazione: "+ generazione);
    }

    public void setStartButton(){
        btn_start.setText("Start");
    }

    private int getIndexOfCanvas(int x, int y){
        int canvasX = (x / (NUMBOFCELL/NUMBOFCANVASPERLINE));
        int canvasY = (y / (NUMBOFCELL / NUMBOFCANVASPERLINE));
        return canvasX+(canvasY*NUMBOFCANVASPERLINE);

    }

    public void printLivingCells(Integer livingCell){
        this.lbl_cellevive.setText("Celle vive: "+ livingCell);
    }

    public static void main(String[] s){


    }



}
