package gameoflife.Performance;

import gameoflife.controller.Controller;
import gameoflife.controller.ControllerImpl;
import gameoflife.controller.MatrixHandlerImpl;

public class SyncronizeMutiThread {

    public static void main(String s[]){
        int cores=Runtime.getRuntime().availableProcessors()+1;


        Controller c1=new ControllerImpl(true,false,false,cores);
        c1.initialize(5000,5000,null);
        System.out.println("Sincronizzazione sul metodo, "+cores+" thread:");
        c1.start(10);

    }
}
