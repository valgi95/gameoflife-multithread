package gameoflife.viewextremecanvas;

public class myPair<E, K> {

    private E x;
    private K y;

    public myPair(E x, K y){
        this.x = x;
        this.y = y;
    }

    public E getX() {
        return x;
    }

    public K getY() {
        return y;
    }

    public void setX(E x) {
        this.x = x;
    }

    public void setY(K y) {
        this.y = y;
    }


    @Override
    public String toString() {
        return "myPair{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
