package gameoflife.viewcanvas;

import com.sun.prism.paint.Color;
import gameoflife.model.MatrixImpl;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;


public class FxControllerCanvas {

    private static final Integer NUMBEROFLINEDEFAULT = 2000;
    private static final Double DEFAULTCELLSIZE = 4.0;

    private static final FxControllerCanvas istance = new FxControllerCanvas();

    private MatrixImpl initialMx;
    private boolean isInitialized = false;


    @FXML
    private Canvas canvas;

    @FXML
    private Button btn_star;

    private GraphicsContext gc;


    @FXML
    private void initialize(){
        if(!this.isInitialized){
            throw new IllegalStateException("Error: Control class not initialized");
        }
        this.canvas.setWidth(NUMBEROFLINEDEFAULT * DEFAULTCELLSIZE);
        this.canvas.setHeight(NUMBEROFLINEDEFAULT * DEFAULTCELLSIZE);
        this.gc = this.canvas.getGraphicsContext2D();
        for(int i = 0; i < NUMBEROFLINEDEFAULT; i++){
            for(int r = 0; r < NUMBEROFLINEDEFAULT; r++){
                if((i+r)%2 == 1) {
                   this.fillCell(r, i);
                }
            }
        }

    }

    public void fillCell(final int x, final int y){

        Double tmpx = x * DEFAULTCELLSIZE;
        Double tmpy = y * DEFAULTCELLSIZE;

        this.gc.fillRect(tmpx, tmpy, DEFAULTCELLSIZE, DEFAULTCELLSIZE);

    }

}
