package gameoflife.model;
/**
 * classe che gestisce una matrice di booleani
 * @author riccardo soro
 *
 */
public interface Matrix {
	
		/**
		 * 
		 * @return true se tutte le celle della matrice sono false
		 */
	    public boolean isAllFalse ();
	    
	    /**
	     * stampa a video la matricesu stdout
	     */
	    public void show() ;
	    
	    /**
	     * genera una matrice MxN con valori casuali
	     * @param M dimensione della matrice MxN da generare
	     * @param N dimensione della matrice MxN da generare
	     * @return la matrice generata randomicamente
	     */
	    public static Matrix randomMatrix(int M, int N) {
			return MatrixImpl.randomMatrix(M, N);
		}
	    
	    /**
	     * genera una matrice MxN con valori tutti falsi
	     * @param M dimensione della matrice MxN da generare
	     * @param N dimensione della matrice MxN da generare
	     * @return la matrice generata
	     */
	    public static Matrix falseMatrix (int M, int N) {
			return MatrixImpl.falseMatrix(M, N);
		}
	    
	    /**
	     * metodo che restituisce la cella della matrice nella coordinata MxN
	     * @param M coordinata della cella
	     * @param N coordinata della cella
	     * @return il valore della cella in coordinate MxN
	     */
	    public boolean getCell (int M, int N);
	    
	    /**
	     * setta il valore di una cella della matrice nella coordinata MxN
	     * @param M coordinata della cella
	     * @param N coordinata della cella
	     * @param value della cella da settare
	     */
	    public void setCell (int M, int N,boolean value);
}
