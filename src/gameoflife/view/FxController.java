package gameoflife.view;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class FxController {

    private static final Integer NUMBEROFLINEDEFAULT = 1000;
    private static final Double HGAP = 5.0;
    private static final Double VGAP = 1.5;

    @FXML
    private GridPane dynamicGridPane;

    @FXML
    private Button btn_start;

    @FXML
    private void initialize() {


        for (int i = 0; i < NUMBEROFLINEDEFAULT; i++) {
            for(int r = 0; r < NUMBEROFLINEDEFAULT; r++) {
                dynamicGridPane.add(new Circle(10), i, r);
            }
        }
        btn_start.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(btn_start.getText().equals("Start")){
                    dynamicGridPane.getChildren().forEach((e) ->{
                        ((Circle)e).setFill(Color.RED);
                    });
                    btn_start.setText("Stop");
                }else{
                    dynamicGridPane.getChildren().forEach((e) ->{
                        ((Circle)e).setFill(Color.GREEN);
                    });
                    btn_start.setText("XD");
                }
            }
        });
    }
}
