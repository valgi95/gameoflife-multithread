package gameoflife.controller;

import gameoflife.model.Matrix;

import java.util.concurrent.Semaphore;

/**
 * questa classe gestisce e controlla le matrici e i calcoli per ottenere le nuove generazioni
 * @author riccardo soro
 *
 */

public interface MatrixHandler {
	   /**
	    * passa alla generazione successiva, calcola quindi a nuova generazione creando piu thread
	    * che calcolano il valore delle celle alla generazione successiva
	    * @throws InterruptedException se il thread viene interrotto
	    */
	   public void next() throws InterruptedException;

	   public void setGenerazioni (int generazioni);
	   /**
	    * metodo per ottenere il valore della cella della matrice attualmente utilizzata
	    * @param m
	    * @param n
	    * @return il valore della cella m,n della matrice attuale
	    */
	   public boolean getCell(final int m, final int n);
	   
	   /**
	    * setta il valore della cella m,n nella prossima generazione
	    * @param m
	    * @param n
	    * @param value che la cella dovr� avere nella prossima generazione
	    */
	   public void setCell(final int m, final int n, final boolean value);
	   
	   /**
	    * questa classe serve ai thread per sapere quale cella calcolare.
	    * @return la cella che il thread deve calcolare
	    */
	   public Integer getNextCellSyncronize() ;
	   
	   /**
	    * 
	    * @return la coordinata M della matrice originale MxN
	    */
	   public int getM();
	   
	   /**
	    * 
	    * @return la coordinata N della matrice originale MxN
	    */
	   public int getN();
	   
	    /**
	     * 
	     * @return la matrice con le celle che rappresentano lo stato attuale
	     */
	    public Matrix getCurrentState();
	    
	    /**
	     * 
	     * @return la matrice con le celle che rappresentano lo stato futuro
	     */
	    public Matrix getNextState();

	    /**
	     * 
	     * @return il numero di volte che � stato eseguito il metodo next()
	     */
	    public int getRound();
	    
	    /**
	     * state0 e state1 si alternano per generare il courrentstate e il nextstate
	     * @param state0
	     */
	    public void setState0(Matrix state0) ;
	    
	    /**
	     * state0 e state1 si alternano per generare il courrentstate e il nextstate
	     * @param state1
	     */
	    public void setState1(Matrix state1) ;
	    
	    /**
	     * setta la dimensione M della matrice MxN
	     * @param m
	     */
	    public void setM(int m) ;
	    
	    /**
	     * setta la dimensione N della matrice MxN
	     * @param n
	     */
	    public void setN(int n) ;
	    
	    /**
	     * 
	     * @param state se � true allora state1 � il courrentstate e state0 � nextstate, altrimenti il contrario
	     */
	    public void setState(boolean state) ;
	    
	    /**
	     * 
	     * @return l'istanza della classe
	     */
	    public static MatrixHandler getIstance(){
	       return MatrixHandlerImpl.getIstance();
	    }
	    
	    /**
	     * inizializza l'istanza della classe
	     * @param m coordinate della matrice MxN
	     * @param n coordinate della matrice MxN
	     * @param randomValue vero se si desidera iniziare con i campi settati in maniera randomica, 
	     * 					  false se si vogliono tutti vuoti
	     */
	    public static void initializeIstance (int m, int n, boolean randomValue) {
	        MatrixHandlerImpl.initializeIstance(m, n, randomValue);
	    }
	    
	    /**
	     * setta il valore del round
	     * @param round 
	     */
	    public void setRound(int round) ;
	    
	    /**
	     * setta delle variabili interne
	     */
	    public void initializeCounter();

	Integer getNextCellMonitor();
	public Boolean getMonitor() ;
	public boolean getRisorsaLibera();
	public void setRisorsaLibera(boolean a);
	public Semaphore getSemaphore();

	Integer getNextCellSemaforo();
}
