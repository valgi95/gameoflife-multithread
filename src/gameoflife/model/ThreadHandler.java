package gameoflife.model;

import gameoflife.viewextremecanvas.FxControllerCanvasExtreme;

public interface ThreadHandler {
	
	/**
	 * 
	 * @return l'istanza della classe
	 */
    public static ThreadHandler getInstance() {
        return ThreadHandlerImpl.getInstance();
    }

	public void resumeWorkerThread();

    /**
     * esegue i thread per calcolare i futuri valori delle celle
     * @throws InterruptedException se il thread viene stoppato
     */
	public void startWorkerThread() ;
	public void setFinish(boolean t);
	public void startThreadController();
	public void stampaVariabili();
	public void stopWorkerThread();
	public void setHandlerView(FxControllerCanvasExtreme fx);
	public static void initializeThreadHandler(FxControllerCanvasExtreme fx,boolean isSyncronize,boolean isSemaphore,
											   boolean isWait, int thread){
		ThreadHandlerImpl.initializeThreadHandler(fx,isSyncronize,isSemaphore,isWait,thread);
	}
	public void setSyncronize(boolean syncronize) ;

	public void setSemaphore(boolean semaphore) ;

	public void setWait(boolean wait) ;

	public void setThread(int thread);
}
