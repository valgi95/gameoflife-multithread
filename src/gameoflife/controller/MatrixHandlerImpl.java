package gameoflife.controller;

import gameoflife.model.Matrix;
import gameoflife.model.ThreadHandler;

import java.util.concurrent.Semaphore;

public class MatrixHandlerImpl implements MatrixHandler{
   private static MatrixHandler istance;
   private Matrix state0;
   private Matrix state1;
   private int m;
   private int n;
   private boolean state;
   private int round;
   private Integer countM;
   private Integer countN;
   private int generazioni=0;
   private static Boolean lock= new Boolean(true);
   private volatile boolean risorsaLibera =true;
   private static Semaphore semaphore = new Semaphore(1,true);

   private MatrixHandlerImpl() {
   }
   public void initializeCounter() {
      this.setCountM(m-1);
      this.setCountN(n-1);
   }

    public Boolean getMonitor() {
        return lock;
    }
    public Semaphore getSemaphore(){
       return semaphore;
    }
    public boolean getRisorsaLibera(){
       return risorsaLibera;
    }
    public void setRisorsaLibera(boolean a){
       this.risorsaLibera =a;
    }

    public void setGenerazioni(int generazioni) {
        this.generazioni = generazioni;
    }
    public void next() {
      state = !state;
      round++;
      initializeCounter();
      if (state) this.state0=Matrix.falseMatrix(this.m,this.n);
      else this.state1=Matrix.falseMatrix(this.m,this.n);
   }

   public boolean getCell(final int m, final int n) {
       return this.getCurrentState().getCell(m,n);
   }

   public void setCell(final int m, final int n, final boolean value) {
      this.getNextState().setCell(m, n, value);

   }

   public synchronized Integer getNextCellSyncronize() {
       int x;
       x=countM;
       if (generazioni==0){
           ThreadHandler.getInstance().setFinish(true);
           return null;
       }
      if (countM == 0){
          this.next();
          this.generazioni--;
      }
      else  countM--;
      //System.out.println("Ritorno "+(x)+" "+(y));
       return x;
   }
    public Integer getNextCellMonitor() {
        int x;
        x=countM;
        if (generazioni==0){
            ThreadHandler.getInstance().setFinish(true);
            synchronized (lock){
                this.risorsaLibera =true;
                lock.notify();
            }
            return null;
        }
        if (countM == 0){
            this.next();
            this.generazioni--;
        }
       else  countM--;
        //System.out.println("Ritorno "+(x)+" "+(y));
        synchronized (lock){
            this.risorsaLibera =true;
            lock.notify();
        }
        return x;
    }
    public Integer getNextCellSemaforo() {
        int x;
        x=countM;
        if (generazioni==0){
            ThreadHandler.getInstance().setFinish(true);
            return null;
        }
        if (countM == 0){
            this.next();
            this.generazioni--;
        }
        else  countM--;
        //System.out.println("Ritorno "+(x)+" "+(y));
        return x;
    }

   public int getM(){
      return this.m;
   }
   public int getN(){
      return this.n;
   }

    public Matrix getCurrentState(){
       if (this.state) return this.state1;
       else return this.state0;
    }

    public Matrix getNextState(){
       if (this.state) return this.state0;
       else return this.state1;
    }


    public int getRound(){
      return this.round;
   }

    public void setState0(Matrix state0) {
        this.state0 = state0;
    }

    public void setState1(Matrix state1) {
        this.state1 = state1;
    }

    public void setM(int m) {
        this.m = m;
    }

    public void setN(int n) {
        this.n = n;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public void setRound(int round) {
        this.round = round;
    }

    private void setCountM(Integer countM) {
        this.countM = countM;
    }

    private void setCountN(Integer countN) {
        this.countN = countN;
    }
    
    
    /**
     * 
     * @return l'istanza della classe
     */
    public static MatrixHandler getIstance(){
       return istance;
    }
    
    /**
     * inizializza l'istanza della classe
     * @param m coordinate della matrice MxN
     * @param n coordinate della matrice MxN
     * @param randomValue vero se si desidera iniziare con i campi settati in maniera randomica, 
     * 					  false se si vogliono tutti vuoti
     */
    public static void initializeIstance (int m, int n, boolean randomValue) {
        istance = new MatrixHandlerImpl();
        if (randomValue) istance.setState0(Matrix.randomMatrix(m, n));
        else istance.setState0(Matrix.falseMatrix(m, n));
        istance.setState1(Matrix.falseMatrix(m, n));
        istance.setState(false);
        istance.setM(m);
        istance.setN(n);
        istance.setRound(0);
        istance.initializeCounter();
    }
}
