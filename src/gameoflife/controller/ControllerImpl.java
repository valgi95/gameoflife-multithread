package gameoflife.controller;

import gameoflife.model.Matrix;
import gameoflife.model.ThreadHandler;
import gameoflife.viewcanvas.FxControllerCanvas;
import gameoflife.viewextremecanvas.FxControllerCanvasExtreme;
import gameoflife.viewextremecanvas.View;
import javafx.application.Platform;
import javafx.fxml.FXML;

public class ControllerImpl implements Controller {
    private int generazioni;
    private int m;
    private int n;
    private boolean start=false;
    private FxControllerCanvasExtreme viewHandler;
    private Integer counterLinvigCells = 0;
    private boolean isSyncronize,isSemaphore,isWait;
    private int thread;

    public ControllerImpl (boolean isSyncronize,boolean isSemaphore,boolean isWait, int thread){
        this.isSemaphore=isSemaphore;
        this.isSyncronize=isSyncronize;
        this.isWait=isWait;
        this.thread=thread;
    }

    private void printFirstRandomMatrix(){
        if (this.viewHandler==null) return;
        Matrix matrix = MatrixHandlerImpl.getIstance().getCurrentState();
        counterLinvigCells = 0;
        for(int y = 0; y < FxControllerCanvasExtreme.NUMBOFCELL; y++){
            // System.out.println("disegn cella "+","+y);
            for(int x = 0; x < FxControllerCanvasExtreme.NUMBOFCELL; x++){

                if(matrix.getCell(y, x)){
                    viewHandler.paint(x,y);
                    counterLinvigCells ++;
                }else{
                    viewHandler.cleanCell(x,y);
                }
            }
        }
        Platform.runLater(()->{
            viewHandler.printLivingCells(counterLinvigCells);
        });
    }

    @Override
    public void initialize(int m, int n, FxControllerCanvasExtreme viewHandler) {
        this.viewHandler = viewHandler;

        //System.out.println("sto inizializzando la matrice");
        MatrixHandler.initializeIstance(m,n,true);
        ThreadHandler.initializeThreadHandler(viewHandler,isSyncronize,isSemaphore,isWait,thread);
        //System.out.println("ho inizializzato la matrice");
        this.m=m;
        this.n=n;
        //System.out.println("sto stampando la matrice");
        printFirstRandomMatrix();
        //System.out.println("ho stampando la matrice");
    }

    @Override
    public void start(int generazioni) {


            start=true;
            //System.out.println("starto Thread");
            this.generazioni = generazioni;
            MatrixHandler.getIstance().setGenerazioni(generazioni);
            ThreadHandler.getInstance().startWorkerThread();
            ThreadHandler.getInstance().stampaVariabili();
           // System.out.println(generazioni);

    }

    @Override
    public void reset() {
        ThreadHandler.getInstance().stopWorkerThread();
    }

    @Override
    public void pause() {
        ThreadHandler.getInstance().stopWorkerThread();
    }

    @Override
    public void resume() {
        ThreadHandler.getInstance().resumeWorkerThread();
    }

    @Override
    public int getRoud() {
        if (MatrixHandler.getIstance()!=null)return MatrixHandler.getIstance().getRound();
        else return 0;
    }


}
