package gameoflife.model;

import gameoflife.controller.MatrixHandler;
import gameoflife.controller.MatrixHandlerImpl;

import gameoflife.viewextremecanvas.FxControllerCanvasExtreme;
import javafx.application.Platform;

import java.util.ArrayList;
import java.util.List;

public class ThreadHandlerImpl extends java.lang.Thread implements ThreadHandler {
    private  volatile boolean start=false;
    private  volatile boolean finish=false;
    private static ThreadHandler istance= new ThreadHandlerImpl();
    private List<ThreadImpl> threads=new ArrayList<>();
    private FxControllerCanvasExtreme handlerView;
    private Integer livingCells = 0;
    private boolean isSyncronize,isSemaphore,isWait;
    private int thread;
    private ThreadHandlerImpl() {
    }
    
    public static ThreadHandler getInstance() {
        return istance;
    }
    private void initializeWorkerThread() {
    	;
        //int cores=12;
        for (int a=0;a<this.thread;a++){
            this.threads.add(new ThreadImpl(a,isSyncronize,isSemaphore,isWait));
        }
    }
    public void setHandlerView(FxControllerCanvasExtreme fx){
        this.handlerView=fx;
    }

    public static void initializeThreadHandler(FxControllerCanvasExtreme fx,boolean isSyncronize,boolean isSemaphore,
                                               boolean isWait, int thread){
        ThreadHandlerImpl.getInstance().setSyncronize(isSyncronize);
        ThreadHandlerImpl.getInstance().setSemaphore(isSemaphore);
        ThreadHandlerImpl.getInstance().setWait(isWait);
        ThreadHandlerImpl.getInstance().setThread(thread);
        ThreadHandlerImpl.getInstance().setHandlerView (fx);
        ThreadHandlerImpl.getInstance().startThreadController();
    }
    public void startWorkerThread()  {
        this.start=true;
        this.finish=false;
    	this.initializeWorkerThread();
        //this.stampaVariabili();
        //ThreadHandler.getInstance().stampaVariabili();

    }
    public void setSyncronize(boolean syncronize) {
        isSyncronize = syncronize;
    }

    public void setSemaphore(boolean semaphore) {
        isSemaphore = semaphore;
    }

    public void setWait(boolean wait) {
        isWait = wait;
    }

    public void setThread(int thread) {
        this.thread = thread;
    }
    public void stampaVariabili(){
        //System.out.println(start+" "+finish);
    }
    public void startThreadController(){
        this.start();

    }
    public void resumeWorkerThread(){
        this.initializeWorkerThread();
        this.start=true;
    }
    
    public void stopWorkerThread() {
        this.start=false;
    	this.threads.forEach(e->{
    		e.stopThread();
    	});
    	this.threads=new ArrayList<>();
    }
    @Override
    public void run() {
        while (true) {

            while (true) {
              if (start && !finish) break;
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            long startTime = System.nanoTime();
            this.threads.forEach(e -> {
                e.start();
            });
            for (ThreadImpl e : this.threads) {
                try {
                    e.join();
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }

            System.out.println((System.nanoTime() - startTime)/1000000+ " millisecondi");
            if (this.handlerView!=null)Platform.runLater(()->{
                this.handlerView.setGenerazioni(MatrixHandler.getIstance().getRound());
                this.handlerView.setStartButton();
            });
            this.stampaVariabili();
            //System.out.println("aggiorno gui");
            this.printCurrentStateMatrix();
            this.threads = new ArrayList<>();
        }
    }

    public void setFinish(boolean t){
        this.finish=t;
    }
    private void printCurrentStateMatrix(){
        if (this.handlerView==null)return;
        Matrix matrix = MatrixHandlerImpl.getIstance().getCurrentState();
        livingCells = 0;
        for(int y = 0; y < FxControllerCanvasExtreme.NUMBOFCELL; y++){
            for(int x = 0; x < FxControllerCanvasExtreme.NUMBOFCELL; x++){
                final int xx = x;
                final int yy = y;
                if(matrix.getCell(y, x)){
                    livingCells ++;
                    Platform.runLater(()->{
                        handlerView.paint(xx,yy);

                    });
                }else{
                    Platform.runLater(()->{
                        handlerView.cleanCell(xx,yy);
                    });
                }
            }
        }
        Platform.runLater(() -> {
            handlerView.printLivingCells(livingCells);
        });
    }


	

}
