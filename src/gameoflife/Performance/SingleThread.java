package gameoflife.Performance;

import gameoflife.controller.Controller;
import gameoflife.controller.ControllerImpl;

public class SingleThread {
    public static void main(String s[]){
        int cores=1;


        Controller c1=new ControllerImpl(true,false,false,cores);
        c1.initialize(5000,5000,null);
        System.out.println("Sequenziale, "+cores+" thread:");
        c1.start(10);
    }
}
